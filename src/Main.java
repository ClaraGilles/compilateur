import java.io.IOException;


public class Main {
    public static void main(String args[] ) throws IOException {
        AnalyseurLexical analyseurLexical = new AnalyseurLexical();
        AnalyseurSyntaxique analyseurSyntaxique = new AnalyseurSyntaxique(analyseurLexical);
        VirtualMachine virtualMachine = new VirtualMachine(analyseurSyntaxique);
        virtualMachine.programmePrincipale();


    }
}
