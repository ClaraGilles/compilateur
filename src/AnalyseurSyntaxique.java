import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class AnalyseurSyntaxique {
    private T_UNILEX unilex;
    private AnalyseurLexical analyseurLexical;
    private ArrayList<String> valDeConstChaine;
    private int nbConstChaine;
    private int derniereAdresseVariableGlobal;
    private ArrayList<Integer> pilop;
    private int somPilop;
    private ArrayList<Integer> pCode;
    private int co;
    private ArrayList<String> keyIdentTable;

    public AnalyseurSyntaxique(AnalyseurLexical analyseurLexical) {
        this.analyseurLexical = analyseurLexical;
    }

    private T_UNILEX analex() throws IOException {
        T_UNILEX t_unilex = analyseurLexical.analex();
        return t_unilex;
    }

    public boolean instruction() throws IOException {
        if (inst_non_cond() || inst_cond()) {
            return true;
        }
        return false;
    }

    private boolean inst_cond() throws IOException {

        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("SI")) {
            unilex = analex();
            if (exp()) {
                pCode.set(co, P_CODE.ALSN.ordinal());
                somPilop++;
                pilop.set(somPilop, co + 1);
                co += 2;
                if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("ALORS")) {
                    unilex = analex();
                    if (inst_cond()) {
                        pCode.set(pilop.get(somPilop), co + 2);
                        somPilop--;
                        pCode.set(co, P_CODE.ALLE.ordinal());
                        somPilop++;
                        pilop.set(somPilop, co + 1);
                        co += 2;
                        pCode.set(pilop.get(somPilop), co + 1);
                        somPilop--;
                        return true;

                    } else if (inst_non_cond()) {
                        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("SINON")) {
                            unilex = analex();
                            pCode.set(pilop.get(somPilop), co + 2);
                            somPilop--;
                            pCode.set(co, P_CODE.ALLE.ordinal());
                            somPilop++;
                            pilop.set(somPilop, co + 1);
                            co += 2;
                            if (instruction()) {
                                pCode.set(pilop.get(somPilop), co + 1);
                                somPilop--;
                                return true;

                            }
                        } else {
                            pCode.set(pilop.get(somPilop), co + 2);
                            somPilop--;
                            pCode.set(co, P_CODE.ALLE.ordinal());
                            somPilop++;
                            pilop.set(somPilop, co + 1);
                            co += 2;
                            pCode.set(pilop.get(somPilop), co + 1);
                            somPilop--;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean inst_non_cond() throws IOException {
        if (affectation() || lecture() || ecriture() || bloc() || inst_repe()) {
            return true;
        }
        return false;
    }

    private boolean inst_repe() throws IOException {
        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("TANTQUE")) {
            unilex = analex();
            somPilop++;
            pilop.set(somPilop, co + 1);


            if (exp()) {
                pCode.set(co, P_CODE.ALSN.ordinal());
                somPilop++;
                pilop.set(somPilop, co + 1);
                co += 2;

                if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("FAIRE")) {
                    unilex = analex();
                    if (instruction()) {
                        pCode.set(pilop.get(somPilop), co + 2);
                        somPilop--;
                        pCode.set(co, P_CODE.ALLE.ordinal());
                        pCode.set(co+1, pilop.get(somPilop));
                        somPilop--;
                        co += 2;
                        return true;
                    }
                }
            }
        }
        return false;
    }


    private boolean affectation() throws IOException {
        if (unilex == T_UNILEX.ident) {
            if (est_declare_var()) {
                pCode.set(co, P_CODE.EMPI.ordinal());
                pCode.set(co+1, analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()).getAdrv());
                co += 2;
                unilex = analex();
                if (unilex == T_UNILEX.aff) {
                    unilex = analex();
                    if (exp()) {
                        pCode.set(co, P_CODE.AFFE.ordinal());
                        co++;
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private boolean lecture() throws IOException {
        boolean erreur;
        boolean fin;
        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("LIRE")) {
            unilex = analex();
            if (unilex == T_UNILEX.parouv) {
                unilex = analex();
                if (unilex == T_UNILEX.ident) {
                    if (!est_declare_var()) return false;
                    pCode.set(co, P_CODE.EMPI.ordinal());
                    pCode.set(co + 1, analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()).getAdrv());
                    pCode.set(co + 2, P_CODE.LIRE.ordinal());
                    co += 3;
                    unilex = analex();
                    fin = false;
                    erreur = false;
                    while (!fin) {
                        if (unilex == T_UNILEX.virg) {
                            unilex = analex();
                            if (unilex == T_UNILEX.ident) {
                                if (!est_declare_var()) return false;
                                pCode.set(co, P_CODE.EMPI.ordinal());
                                pCode.set(co + 1, analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()).getAdrv());
                                pCode.set(co + 2, P_CODE.LIRE.ordinal());
                                co += 3;
                                unilex = analex();
                            } else {
                                fin = true;
                                erreur = true;
                            }
                        } else {
                            fin = true;
                        }
                    }
                    if (erreur) {
                        return false;
                    } else if (unilex == T_UNILEX.parfer) {
                        unilex = analex();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean ecriture() throws IOException {
        boolean fin;
        boolean erreur;
        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("ECRIRE")) {
            unilex = analex();
            if (unilex == T_UNILEX.parouv) {
                unilex = analex();
                erreur = false;
                fin = false;
                if (ecr_exp()) {
                    while (!fin) {
                        if (unilex == T_UNILEX.virg) {
                            unilex = analex();
                            erreur = !ecr_exp();
                            if (erreur) {
                                fin = true;
                            }
                        } else {
                            fin = true;
                        }
                    }
                } else {
                    pCode.set(co, P_CODE.ECRL.ordinal());
                    co++;
                }
                if (erreur) {
                    return false;
                } else if (unilex == T_UNILEX.parfer) {
                    unilex = analex();
                    return true;
                }
            }
        }
        return false;
    }

    private boolean op_bin() throws IOException {
        if (unilex == T_UNILEX.plus || unilex == T_UNILEX.moins || unilex == T_UNILEX.mult || unilex == T_UNILEX.divi) {
            somPilop++;
            if (unilex == T_UNILEX.plus) {
                pilop.set(somPilop, P_CODE.ADDI.ordinal());
            } else if (unilex == T_UNILEX.moins) {
                pilop.set(somPilop, P_CODE.SOUS.ordinal());
            } else if (unilex == T_UNILEX.mult) {
                pilop.set(somPilop, P_CODE.MULT.ordinal());
            } else {
                pilop.set(somPilop, P_CODE.DIVI.ordinal());
            }
            unilex = analex();
            return true;
        } else return false;
    }

    private boolean terme() throws IOException {
        if (unilex == T_UNILEX.ent || unilex == T_UNILEX.ident) {
            if (unilex == T_UNILEX.ident) {
                if (analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()) == null || analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()).getTypec() == 1) {
                    return false;
                }
                pCode.set(co, P_CODE.EMPI.ordinal());
                pCode.set(co + 1, analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()).getAdrv());
                pCode.set(co + 2, P_CODE.CONT.ordinal());
                co += 3;
            } else {
                pCode.set(co, P_CODE.EMPI.ordinal());
                pCode.set(co + 1, analyseurLexical.getNombre());
                co += 2;
            }
            unilex = analex();
            return true;
        }
        if (unilex == T_UNILEX.parouv) {
            unilex = analex();
            if (exp()) {
                if (unilex == T_UNILEX.parfer) {
                    unilex = analex();
                    return true;
                }
            }
        }
        if (unilex == T_UNILEX.moins) {
            if (terme()) {
                pCode.set(co, P_CODE.MOIN.ordinal());
                co++;
                return true;
            }
        }
        return false;
    }

    private boolean suite_terme() throws IOException {
        if (op_bin()) {
            if (exp()) {

                pCode.set(co, pilop.get(somPilop));
                somPilop--;
                co++;
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private boolean exp() throws IOException {
        if (terme()) {
            if (suite_terme()) {
                return true;
            }
        }
        return false;
    }

    private boolean ecr_exp() throws IOException {
        if (exp()) {
            pCode.set(co, P_CODE.ECRE.ordinal());
            co++;
            return true;
        }
        if (unilex == T_UNILEX.ch) {
            pCode.set(co, P_CODE.ECRC.ordinal());
            for (int i = 0; i < analyseurLexical.getChaine().length(); i++) {
                pCode.set(co + i + 1, (int) analyseurLexical.getChaine().charAt(i));
            }
            pCode.set(co + analyseurLexical.getChaine().length() + 1, P_CODE.FINC.ordinal());
            co += analyseurLexical.getChaine().length() + 2;
            unilex = analex();
            return true;
        }
        return false;
    }

    private boolean bloc() throws IOException {
        boolean fin;
        boolean erreur;
        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("DEBUT")) {
            unilex = analex();
            erreur = false;

            if (instruction()) {
                fin = false;
                while (!fin) {
                    if (unilex == T_UNILEX.ptvirg) {
                        unilex = analex();
                        erreur = !instruction();
                        if (erreur) {
                            fin = true;
                        }
                    } else {
                        fin = true;
                    }
                }
            }
            if (erreur) {
                return false;
            } else if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("FIN")) {
                unilex = analex();
                return true;
            }
        }
        return false;
    }

    private boolean decl_var() throws IOException {
        boolean erreur;
        boolean fin;
        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("VAR")) {
            unilex = analex();
            if (unilex == T_UNILEX.ident) {
                String nomVariable = analyseurLexical.getChaine();
                if (definir_variable(nomVariable)) {
                    unilex = analex();
                    fin = false;
                    erreur = false;
                    while (!fin) {
                        if (unilex == T_UNILEX.virg) {
                            unilex = analex();
                            if (unilex == T_UNILEX.ident) {
                                String nomVariable2 = analyseurLexical.getChaine();
                                if (definir_variable(nomVariable2)) {
                                    unilex = analex();
                                } else {
                                    fin = true;
                                    erreur = true;
                                }
                            } else {
                                fin = true;
                                erreur = true;
                            }
                        } else {
                            fin = true;
                        }
                    }
                    if (erreur) {
                        return false;
                    } else if (unilex == T_UNILEX.ptvirg) {
                        unilex = analex();
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private boolean decl_const() throws IOException {
        boolean fin;
        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("CONST")) {
            unilex = analex();
            if (unilex == T_UNILEX.ident) {
                String nomConstante = analyseurLexical.getChaine();
                unilex = analex();
                if (unilex == T_UNILEX.eg) {
                    unilex = analex();
                    if (unilex == T_UNILEX.ent || unilex == T_UNILEX.ch) {
                        if (definir_constante(nomConstante)) {
                            unilex = analex();
                            fin = false;
                            while (!fin) {
                                if (unilex == T_UNILEX.virg) {
                                    unilex = analex();
                                    if (unilex == T_UNILEX.ident) {
                                        nomConstante = analyseurLexical.getChaine();
                                        unilex = analex();
                                        if (unilex == T_UNILEX.eg) {
                                            unilex = analex();
                                            if (unilex == T_UNILEX.ent || unilex == T_UNILEX.ch) {
                                                if (definir_constante(nomConstante)) {
                                                    unilex = analex();
                                                    fin = false;
                                                } else {
                                                    fin = true;
                                                }
                                            } else {
                                                fin = true;
                                            }
                                        } else {
                                            fin = true;
                                        }
                                    } else {
                                        fin = true;
                                    }
                                } else {
                                    fin = true;
                                }
                            }
                            if (unilex == T_UNILEX.ptvirg) {
                                unilex = analex();
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    private boolean definir_constante(String nom) {
        Identificateur enreg = new Identificateur();

        if (analyseurLexical.getIdentificateurTable().chercher(nom) != null) {
            return false;
        } else {
            enreg.setType("CONST");
            if (unilex == T_UNILEX.ent) {
                enreg.setTypec(0);
                enreg.setValeur(analyseurLexical.getNombre());
            } else if (unilex == T_UNILEX.ch) {
                enreg.setTypec(1);
                valDeConstChaine.add(analyseurLexical.getChaine());
                enreg.setValeur(nbConstChaine++);
            }
            analyseurLexical.getIdentificateurTable().inserer(nom, enreg);
            keyIdentTable.add(nom);
            return true;
        }
    }

    private boolean definir_variable(String nom) {
        Identificateur enreg = new Identificateur();

        if (analyseurLexical.getIdentificateurTable().chercher(nom) != null) {
            return false;
        } else {
            enreg.setType("VAR");
            derniereAdresseVariableGlobal++;
            enreg.setAdrv(derniereAdresseVariableGlobal);
            if (unilex == T_UNILEX.ent) {
                enreg.setTypec(0);
                enreg.setValeur(analyseurLexical.getNombre());
            }
            analyseurLexical.getIdentificateurTable().inserer(nom, enreg);
            keyIdentTable.add(nom);
            return true;
        }


    }

    private boolean est_declare_var() {
        if (analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()) != null) {
            if (analyseurLexical.getIdentificateurTable().chercher(analyseurLexical.getChaine()).getType().equals("VAR")) {
                return true;
            }
        }
        return false;
    }

    private boolean prog() throws IOException {
        if (unilex == T_UNILEX.motcle && analyseurLexical.getChaine().equals("PROGRAMME")) {
            unilex = analex();
            if (unilex == T_UNILEX.ident) {
                unilex = analex();
                if (unilex == T_UNILEX.ptvirg) {
                    unilex = analex();
                    decl_const();
                    decl_var();
                    if (bloc()) {
                        if (unilex == T_UNILEX.point) {
                            pCode.set(co,P_CODE.STOP.ordinal());
                            return true;
                        }
                    }

                }
            }
        }
        return false;
    }


    public void anasynt() throws IOException {
        unilex = analex();
        if (prog()) {
            System.out.println("Le programme est syntaxiquement correct");
        } else {
            analyseurLexical.erreur(3);
        }
    }

    public void initialiser() throws FileNotFoundException {
        analyseurLexical.initialiser();
        nbConstChaine = 0;
        valDeConstChaine = new ArrayList<>();
        derniereAdresseVariableGlobal = -1;
        pilop = new ArrayList<>();
        pCode = new ArrayList<>();
        for (int i = 0; i <= 1000; i++) {
            pilop.add(null);
            pCode.add(null);
        }
        somPilop = -1;
        co = 0;

        this.keyIdentTable = new ArrayList<>();


    }

    public void terminer() throws IOException {
        analyseurLexical.terminer();

    }

    public void programme_principal() throws IOException {
        initialiser();
        analyseurLexical.lire_Car();
        anasynt();
        terminer();
    }

    public ArrayList<Integer> getpCode() {
        return pCode;
    }

    public ArrayList<String> getKeyIdentTable() {
        return keyIdentTable;
    }

    public AnalyseurLexical getAnalyseurLexical() {
        return analyseurLexical;
    }
}
