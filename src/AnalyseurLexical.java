import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class AnalyseurLexical {

    private static final int LONG_MAX_IDENT = 20;
    private static final int LONG_MAX_CHAINE = 50;
    private static final int NB_MOTS_RESERVES = 12;
    private static final double MAX_INT = Double.MAX_VALUE;

    private IdentificateurTable identificateurTable = new IdentificateurTable();

    private File source;
    private char carlu; //dernier charactère lu
    private int nombre; //dernier nombre lu
    private String chaine; //derniere chaine lu
    private int num_ligne;
    private ArrayList<String> mots_reserves = new ArrayList<>(NB_MOTS_RESERVES);

    private Scanner scanner;
    FileReader reader;

    public int getNombre() {
        return nombre;
    }

    public IdentificateurTable getIdentificateurTable() {
        return identificateurTable;
    }

    public void erreur(int numErreur) {
        if (numErreur == 0) {
            System.out.println("Erreur Lexicale Ligne : " + num_ligne);
            System.exit(0);
        } else if (numErreur == 1) {
            System.out.println("fin de fichier atteinte");
        } else if (numErreur == 2) {
            System.out.println("Erreur Semantique Ligne : " + num_ligne);
            System.exit(0);
        } else if (numErreur == 3) {
            System.out.println("Erreur Syntaxique Ligne : " + num_ligne);
            System.exit(0);
        } else {
            System.out.println("Erreur Inconnue Ligne : " + num_ligne);
            System.exit(0);
        }
    }

    public void lire_Car() throws IOException {
        int currentCar = reader.read();
        if (currentCar == '\n') {
            num_ligne++;

        }
        if (currentCar == -1) {
            erreur(1);
        }
        carlu = (char) currentCar;


    }

    // à verifier recursif
    public void sauter_Separateurs() throws IOException {
        while (carlu == ' ' || carlu == '\r' || carlu == '\t' || carlu == '{' || carlu == '\n') {
            if (carlu == '{') {
                automate_Commentaire();
            }
            lire_Car();
        }
    }

    private void automate_Commentaire() throws IOException {
        while (carlu != '}') {
            lire_Car();
            if (carlu == '{') {
                automate_Commentaire();
                lire_Car();
            }
        }
    }

    public T_UNILEX reco_Entier() throws IOException {
        nombre = 0;
        while (est_un_nombre(carlu)) {
            nombre = nombre * 10 + Character.getNumericValue(carlu);
            if (nombre > MAX_INT) {
                erreur(0);
                //modif : on met quoi là ?
            }
            lire_Car();
        }
        return T_UNILEX.ent;
    }

    public T_UNILEX reco_Chaine() throws IOException {
        lire_Car();
        chaine = "";
        while (true) {
            chaine = chaine + carlu;
            lire_Car();
            if (carlu == '\'') {
                lire_Car();
                if (carlu == '\'') {
                    chaine = chaine + carlu + carlu;
                    lire_Car();
                } else {
                    break;
                }
            }
        }

        if (chaine.length() > LONG_MAX_CHAINE) {
            erreur(0);
        }

        return T_UNILEX.ch;
    }

    public T_UNILEX reco_ident_ou_mot_reserve() throws IOException {
        chaine = carlu + "";
        lire_Car();
        while (est_un_nombre(carlu) || est_une_lettre(carlu) || carlu == '_') {
            chaine = chaine + carlu;
            if (chaine.length() >= LONG_MAX_IDENT) {
                ignore_cara(carlu);
            }
            lire_Car();
        }

        chaine = chaine.toUpperCase();
        if (est_un_mot_reserve(chaine)) {
            return T_UNILEX.motcle;
        }

        return T_UNILEX.ident;
    }

    public void ignore_cara(char cara) throws IOException {
        while (est_un_nombre(cara) || est_une_lettre(cara) || carlu == '_') {
            lire_Car();
        }
    }

    public boolean est_un_mot_reserve(String ch) {
        if (mots_reserves.contains(ch)) {
            return true;
        }
        return false;
    }

    public T_UNILEX reco_symb() throws IOException {
        if (carlu == ';') {
            lire_Car();
            return T_UNILEX.ptvirg;
        } else if (carlu == '+') {
            lire_Car();
            return T_UNILEX.plus;
        } else if (carlu == '-') {
            lire_Car();
            return T_UNILEX.moins;
        } else if (carlu == '*') {
            lire_Car();
            return T_UNILEX.mult;
        } else if (carlu == '/') {
            lire_Car();
            return T_UNILEX.divi;
        } else if (carlu == '<') {
            lire_Car();
            if (carlu == '=') {
                lire_Car();
                return T_UNILEX.infe;
            } else if (carlu == '>') {
                lire_Car();
                return T_UNILEX.diff;
            } else {
                lire_Car();
                return T_UNILEX.inf;
            }
        } else if (carlu == '>') {
            lire_Car();
            if (carlu == '=') {
                lire_Car();
                return T_UNILEX.supe;
            } else {
                lire_Car();
                return T_UNILEX.sup;
            }
        } else if (carlu == '(') {
            lire_Car();
            return T_UNILEX.parouv;
        } else if (carlu == ')') {
            lire_Car();
            return T_UNILEX.parfer;
        } else if (carlu == ',') {
            lire_Car();
            return T_UNILEX.virg;
        } else if (carlu == '=') {
            lire_Car();
            return T_UNILEX.eg;
        } else if (carlu == '.') {
            lire_Car();
            return T_UNILEX.point;
        } else if (carlu == ':') {
            lire_Car();
            if (carlu == '=') {
                lire_Car();
                return T_UNILEX.aff;
            }
            return T_UNILEX.deuxpts;
        } else if (carlu == '!') {
            lire_Car();
            if (carlu == '=') {
                lire_Car();
                return T_UNILEX.diff;
            }
        }
        return null;
    }

    public boolean est_un_nombre(char cara) {
        if (cara >= '0' && cara <= '9')
            return true;

        return false;
    }

    public boolean est_une_lettre(char cara) {
        if (cara >= 'a' && cara <= 'z')
            return true;
        if (cara >= 'A' && cara <= 'Z')
            return true;
        if (cara == '_')
            return true;

        return false;
    }

    public T_UNILEX analex() throws IOException {
        if (carlu == ' ' || carlu == '\r' || carlu == '\t' || carlu == '{' || carlu == '\n') {
            sauter_Separateurs();
        }
        if (est_un_nombre(carlu)) {
            return reco_Entier();
        } else if (carlu == '\'') {
            return reco_Chaine();
        } else if (est_une_lettre(carlu)) {
            return reco_ident_ou_mot_reserve();
        } else {
            return reco_symb();
        }
    }

    public void initialiser() throws FileNotFoundException {
        num_ligne = 1;
        source = new File("src/triangledefibonacci");
        reader = new FileReader(source);

        insere_table_mots_reserves("PROGRAMME");
        insere_table_mots_reserves("DEBUT");
        insere_table_mots_reserves("FIN");
        insere_table_mots_reserves("CONST");
        insere_table_mots_reserves("VAR");
        insere_table_mots_reserves("ECRIRE");
        insere_table_mots_reserves("LIRE");
        insere_table_mots_reserves("SI");
        insere_table_mots_reserves("ALORS");
        insere_table_mots_reserves("SINON");
        insere_table_mots_reserves("TANTQUE");
        insere_table_mots_reserves("FAIRE");
    }

    public void insere_table_mots_reserves(String mot) {
        mots_reserves.add(mot);
        Collections.sort(mots_reserves);

    }

    public void terminer() throws IOException {
        reader.close();
    }

    public void programme_principal() throws IOException {
        initialiser();
        lire_Car();
        terminer();
        identificateurTable.affiche_table_ident();
    }

    public String getChaine() {
        return chaine;
    }

}
