import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class VirtualMachine {

    private final int tailleMaxMemoire = 10000;
    private ArrayList<Integer> memoire;

    private ArrayList<Integer> pilex;
    private int somPilex;
    private int co;

    private AnalyseurSyntaxique analyseurSyntaxique;

    public VirtualMachine(AnalyseurSyntaxique analyseurSyntaxique) {
        this.analyseurSyntaxique = analyseurSyntaxique;
    }


    public void programmePrincipale() throws IOException {
        analyseurSyntaxique.programme_principal();
        initialiser();
        terminer();
        creerFichierCode();
        interpreter();

    }

    public void initialiser() {
        co = 0;
        memoire = new ArrayList<>();
        pilex = new ArrayList<>();
        somPilex = -1;
    }


    public void terminer() {
    }

    public void creerFichierCode() {
        String fileName = "programme.COD.txt";
        ArrayList<String> spCode = new ArrayList<>();
        for (Integer i : analyseurSyntaxique.getpCode()) {
            if(i != null) {
                spCode.add(String.valueOf(i));
            }
        }
        try {
            Path file = Paths.get(fileName);
            Files.write(file, spCode, StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }


    public void interpreter() {
        Scanner scanner = new Scanner(System.in);
        while (analyseurSyntaxique.getpCode().get(co) != 13) {
            switch (analyseurSyntaxique.getpCode().get(co)) {
                case 0: //ADDI ;
                    pilex.set(somPilex - 1, pilex.get(somPilex - 1) + pilex.get(somPilex));
                    somPilex--;
                    co++;
                    break;

                case 1: //SOUS
                    pilex.set(somPilex - 1, pilex.get(somPilex - 1) - pilex.get(somPilex));
                    somPilex--;
                    co++;
                    break;

                case 2: //MULT
                    pilex.set(somPilex - 1, pilex.get(somPilex - 1) * pilex.get(somPilex));
                    somPilex--;
                    co++;
                    break;

                case 3: //DIVI
                    if (pilex.get(somPilex) == 0) {
                        System.out.println("division par 0");
                        System.exit(0);
                    } else {
                        pilex.set(somPilex - 1, pilex.get(somPilex - 1) / pilex.get(somPilex));
                        somPilex--;
                        co++;
                    }
                    break;

                case 4: //MOIN
                    pilex.set(somPilex, -pilex.get(somPilex));
                    co++;
                    break;

                case 5: //AFFE
                    analyseurSyntaxique.getAnalyseurLexical().getIdentificateurTable().chercher(analyseurSyntaxique.getKeyIdentTable().get(pilex.get(somPilex - 1))).setValeur(pilex.get(somPilex));
                    somPilex -= 2;
                    co++;
                    break;

                case 6: //LIRE
                    int j = Integer.parseInt(scanner.nextLine());
                    analyseurSyntaxique.getAnalyseurLexical().getIdentificateurTable().chercher(analyseurSyntaxique.getKeyIdentTable().get(pilex.get(somPilex))).setValeur(j);
                    somPilex--;
                    co++;
                    break;

                case 7: //ECRL
                    System.out.println();
                    co++;
                    break;

                case 8: //ECRE
                    System.out.print(pilex.get(somPilex));
                    somPilex--;
                    co++;
                    break;

                case 9: //ECRC
                    int i = 1;
                    String ch = Character.toString(analyseurSyntaxique.getpCode().get(co + i));
                    while (!ch.equals(Character.toString(10))) {
                        System.out.print(ch);
                        i++;
                        ch = Character.toString(analyseurSyntaxique.getpCode().get(co + i));
                    }
                    co += i + 1;
                    break;

                case 11: //EMPI
                    somPilex++;
                    pilex.add(somPilex, analyseurSyntaxique.getpCode().get(co + 1));
                    co += 2;
                    break;

                case 12: //CONT
                    pilex.set(somPilex, analyseurSyntaxique.getAnalyseurLexical().getIdentificateurTable().chercher(analyseurSyntaxique.getKeyIdentTable().get(pilex.get(somPilex))).getValeur());
                    co++;
                    break;

                case 13: //STOP
                    break;

                case 14: //ALLE
                    co = analyseurSyntaxique.getpCode().get(co+1)-1;
                    break;

                case 15: //ALSN
                    if (pilex.get(somPilex) == 0) {
                        co = analyseurSyntaxique.getpCode().get(co+1);
                    } else {
                        co+=2;
                        somPilex--;
                    }
                    break;

                default:
                    System.out.println("mauvaise interprétation");
                    System.exit(0);
            }
        }
    }

}
