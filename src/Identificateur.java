public class Identificateur {
    private String type;
    private int valeur;
    private int typec;
    private int adrv;

    public int getTypec() {
        return typec;
    }

    public int getAdrv() {
        return adrv;
    }

    public String getType() {
        return type;
    }

    public int getValeur() {
        return valeur;
    }


    public void setType(String type) {
        this.type = type;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public void setTypec(int typec) {
        this.typec = typec;
    }

    public void setAdrv(int adrv) {
        this.adrv = adrv;
    }

    public void affiche_ident() {
        System.out.print(typec + " " + type + " " + valeur + " " + adrv);
    }
}
