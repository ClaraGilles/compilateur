import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class IdentificateurTable {

    HashMap<String, Identificateur> indexTable = new HashMap<>();

    public Identificateur chercher(String nom) {
        return indexTable.get(nom);
    }

    public int inserer(String nom, Identificateur genre) {
        if(!indexTable.containsKey(nom)){
            indexTable.put(nom, genre);

        } else {
            System.out.println("Identificateur déjà déclaré");
            System.exit(0);
        }
        return indexTable.size();
    }

    public void affiche_table_ident() {
        for (Map.Entry<String, Identificateur> entry : indexTable.entrySet()
        ) {
            System.out.print(entry.getKey() + " ");
            entry.getValue().affiche_ident();
            System.out.println();

        }
    }

    public boolean contient(String nom){
        return indexTable.containsKey(nom);
    }



}
